package strategy;

import java.util.List;

/**
 * @author WangSen
 * @Title MyCollections
 * @Description
 * @date: 2019-1-18 9:42
 */
public class MyCollections {

    public static void sort(List<MyComparable> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = 0; j < list.size() - i - 1; j++) {
                if (list.get(i).comparable(list.get(i + 1)) == 1) {
                    MyComparable ob = list.get(i);
                    list.set(i,list.get(i+1));
                    list.set(i+1,ob);
                }
            }
        }
    }

    public static void sort(List list,MyComparator catMyComparator){
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = 0; j < list.size() - i - 1; j++) {
                if (catMyComparator.comparable(list.get(i),list.get(i+1))==1) {
                    Object ob = list.get(i);
                    list.set(i,list.get(i+1));
                    list.set(i+1,ob);
                }
            }
        }

    }

}










