package strategy;

/**
 * @author WangSen
 * @Title CatComparator
 * @Description
 * @date: 2019-1-18 10:14
 */
public class CatComparator implements MyComparator<Cat> {
    @Override
    public int comparable(Cat ob1, Cat ob2) {
//        if(ob1.getAge()>ob2.getAge()){
//            return 1;
//        }else {
//            return 0;
//        }
        return ob1.comparable(ob2);
    }
}