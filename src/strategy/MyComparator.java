package strategy;

/**
 * @author WangSen
 * @Title MyComparator
 * @Description
 * @date: 2019-1-18 10:12
 */
public interface MyComparator<T> {
    int comparable(T ob1,T ob2);
}