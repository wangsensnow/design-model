package strategy;

/**
 * @author WangSen
 * @Title MyComparable
 * @Description
 * @date: 2019-1-18 9:40
 */
public interface MyComparable<T> {
    int comparable(T ob);
}