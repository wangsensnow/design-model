package strategy;

/**
 * @author WangSen
 * @Title Cat
 * @Description
 * @date: 2019-1-18 9:39
 */
public class Cat implements MyComparable<Cat> {
    Integer age;

    public Cat(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public int comparable(Cat ob) {
        if (this.getAge() > ob.getAge()) {
            return 1;
        }else {
            return 0;
        }
    }
}