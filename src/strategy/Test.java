package strategy;

import java.util.ArrayList;
import java.util.List;

/**
 * @author WangSen
 * @Title Test
 * @Description
 * @date: 2019-1-18 10:08
 */
public class Test {
    public static void main(String[] args) {
        Cat cat = new Cat(2);
        Cat cat1 = new Cat(1);
        Cat cat2 = new Cat(3);
        List list = new ArrayList();
        list.add(cat);
        list.add(cat1);
        list.add(cat2);
//        MyCollections.sort(list);

        MyComparator<Cat> comparator = new CatComparator();
        MyCollections.sort(list,comparator);

        System.out.println(list);




    }
}